#!/bin/bash
#SBATCH -J myjob
#SBATCH -A <your account name>  # e.g., m1111
#SBATCH -q regular
#SBATCH -t 6:00:00        
#SBATCH -N 2           
#SBATCH -C gpu
#SBATCH -G 8 
#SBATCH --exclusive
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err

module load vasp/6.2.1-gpu

srun -n8 -c32 --cpu-bind=cores --gpu-bind=single:1 -G 8 vasp_std





