#!/bin/bash 
#SBATCH -J test
#SBATCH -q flex 
#SBATCH -N 2             
#SBATCH -C knl
#SBATCH -t 48:00:00 
#SBATCH -e %x-%j.err 
#SBATCH -o %x-%j.out
#SBATCH -time-min=02:00:00  
#
#SBATCH --comment=48:00:00
#SBATCH --signal=B:USR1@300
#SBATCH --requeue
#SBATCH --open-mode=append

module load mana nersc_cr

#checkpointing once every hour
mana_coordinator -i 3600

#c/r jobs
if [[ $(restart_count) == 0 ]]; then

    #user setting
    export OMP_PROC_BIND=spread
    export OMP_PLACES=threads
    export OMP_NUM_THREADS=8
    srun -n 16 -c 32 --cpu-bind=cores mana_launch ./a.out &
elif [[ $(restart_count) > 0 ]] && [[ -e dmtcp_restart_script.sh ]]; then

    srun -n 16 -c 32 --cpu-bind=cores mana_restart &
else

    echo "Failed to restart the job, exit"; exit
fi

# requeueing the job if remaining time >0
ckpt_command=ckpt_mana     #additional checkpointing right before the job hits the walllimit
requeue_job func_trap USR1

wait

