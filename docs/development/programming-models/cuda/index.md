# CUDA

!!! warning
    This page is currently under active development. Check
    back soon for more content.

CUDA is a general purpose parallel computing platform and programming
model that leverages the parallel compute engine in NVIDIA GPUs to
solve many complex computational problems in a more efficient way
than on a CPU.

For full documentation:

-  [CUDA C++ Programming
   Guide](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)
-  [NVIDIA CUDA Fortran Programming
   Guide](https://docs.nvidia.com/hpc-sdk/compilers/cuda-fortran-prog-guide/)

## CUDA C

A vector addition example written in CUDA C is provided in
[this](https://devblogs.nvidia.com/easy-introduction-cuda-c-and-c/) NVIDIA
blog and can be compiled with the `nvcc` compiler provided in the
`PrgEnv-nvidia` environment on Perlmutter.

```slurm
nvcc -o saxpy.ex saxpy.cu
```

## CUDA Fortran

A vector addition example written in CUDA Fortran is provided in
[this](https://devblogs.nvidia.com/easy-introduction-cuda-fortran/) NVIDIA
blog and can be compiled with the `nvfortran` compiler provided by
any `nvhpc` module on Cori GPU:

```slurm
nvfortran -o saxpy.ex saxpy.cuf
```

[comment]: <> (## More CUDA examples)
[comment]: <> ()
[comment]: <> (The CUDA SDK, provided by the `cuda` modules, also provides a large number of)
[comment]: <> (example codes. They are located in `$CUDA_HOME/samples` and are sorted by)
[comment]: <> (category into various subdirectories, e.g., `0_Simple`, `1_Utilities`, etc. One)
[comment]: <> (can copy these sample codes into a write-accessible location and compile the)
[comment]: <> (examples:)
[comment]: <> ()
[comment]: <> (```slurm)
[comment]: <> (cd $HOME)
[comment]: <> (mkdir CUDA_samples)
[comment]: <> (cp -r $CUDA_HOME/samples/{common,7_CUDALibraries} CUDA_samples)
[comment]: <> (cd CUDA_samples/7_CUDALibraries)
[comment]: <> (make)
[comment]: <> (```)
[comment]: <> ()
[comment]: <> (which will create the executable in the directory)
[comment]: <> (`$HOME/CUDA_samples/bin/x86_64/linux/release`.)

## Preparing for Perlmutter

For info on CUDA for Perlmutter, please see the [Native CUDA
C/C++](../../../performance/readiness.md#native-cuda-cc) and [Memory
Management](../../../performance/readiness.md#memory-management),
and other sections in the Perlmutter Readiness page.

## Tutorials

-  CUDA Training Series, 2020:
    -  [Part 1: Introduction to CUDA C++, January 15,
       2020](https://www.nersc.gov/users/training/events/introduction-to-cuda-c-part-1-of-9-cuda-training-series/)
    -  [Part 2: CUDA Shared Memory, February 19,
       2020](https://www.nersc.gov/users/training/events/cuda-shared-memory-part-2-of-9-cuda-training-series/)
    -  [Part 3: Fundamental CUDA Optimization (Part 1), March 18,
       2020](https://www.nersc.gov/users/training/events/fundamental-cuda-optimization-part-1-part-3-of-9-cuda-training-series/)
    -  [Part 4: Fundamental CUDA Optimization (Part 2), April 16,
       2020](https://www.nersc.gov/users/training/events/fundamental-cuda-optimization-part-2-part-4-of-9-cuda-training-series/)
    -  [Part 5: CUDA Atomics, Reductions, and Warp Shuffle, May 13,
       2020](https://www.nersc.gov/users/training/events/cuda-atomics-reductions-and-warp-shuffle-part-5-of-9-cuda-training-series/)
    -  [Part 6: Managed Memory, June 18,
       2020](https://www.nersc.gov/users/training/events/managed-memory-part-6-of-9-cuda-training-series-june-18-2020/)
    -  [Part 7: CUDA Concurrency, July 21,
       2020](https://www.nersc.gov/users/training/events/cuda-concurrency-part-7-of-9-cuda-training-series-july-21-2020/)
    -  [Part 8: GPU Performance Analysis, August 18,
       2020](https://www.nersc.gov/users/training/events/gpu-performance-analysis-part-8-of-9-cuda-training-series-august-18-2020/)
    -  [Part 9: Cooperative Groups, September 17,
       2020](https://www.nersc.gov/users/training/events/cooperative-groups-part-9-of-9-cuda-training-series-september-17-2020/)
    -  [Part 10: CUDA Multithreading with Streams, July 16,
       2021](https://www.nersc.gov/users/training/events/cuda-multithreading-with-streams-july-16-2021/)
    -  [Part 11: CUDA Muti Process Service, August 17,
       2021](https://www.nersc.gov/users/training/events/cuda-multi-process-service-aug-2021/)
    -  [Part 12: CUDA Debugging, September 14,
       2021](https://www.nersc.gov/users/training/events/cuda-debugging-sep-2021/)
    -  [Part 13: CUDA Graphs, October 13,
       2021](https://www.nersc.gov/users/training/events/cuda-graphs-october-13-2021/)

-  [An Easy Introduction to CUDA Fortran](https://developer.nvidia.com/blog/easy-introduction-cuda-fortran/)
-  [An Easy Introduction to CUDA C and C++](https://developer.nvidia.com/blog/easy-introduction-cuda-c-and-c/)
-  [An Even Easier Introduction to CUDA](https://developer.nvidia.com/blog/even-easier-introduction-cuda/)
