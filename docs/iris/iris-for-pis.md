# Iris Guide for PIs and Project Managers

## Project Roles

Traditionally, only PIs and PI proxies used to manage their projects.
With Iris, NERSC introduced two additional project management roles,
Project Resource Managers and Project Membership Managers, for more
efficient management of large projects involving multiple institutions.
Some of the tasks performed by PIs or PI proxies can be delegated
further.  PIs, PI Proxies, Project Resource Managers and Project
Membership Managers, collectively called "project managers," can
work together in managing users and computer resources for their
project.  It is entirely up to PIs, however, to have all, some or
none of the PI Proxy, Project Resource Manager, and Project Membership
Manager roles created for their projects.

The table below summarizes what project members can do in Iris with
their projects:

| | PI | PI Proxy | Project Resource Manager | Project Membership Manager | User |
| ----------- |:---:|:---:|:---:|:---:|:---:|
| Accept Users                                                             | X | X | - | - | - |
| Change Project Role                                                      | X | X | - | - | - |
| Edit Allocation (e.g., allot CPU/GPU Node Hours, enable the premium QOS) | X | X | X | - | - |
| Edit Group (create a Unix group)                                         | X | X | X | X | - |
| Edit HPSS Allocs                                                         | X | X | X | - | - |
| Edit Members (e.g., add a member to a Unix group)                        | X | X | - | X | - |
| Edit Organizations                                                       | X | - | - | - | - |
| Edit Project Details                                                     | X | X | - | - | - |
| Edit Quotas ([Community File System](../filesystems/community.md))       | X | X | - | - | - |
| Invite User                                                              | X | X | - | X | - |
| Mark Continuing Users                                                    | X | X | - | X | - |
| View Project                                                             | X | X | X | X | X |

---

## Navigating Around Iris

This section is to provide quick information on the top-level Iris
menus only.  More detailed information on how to navigate or use
various utilities in Iris can be found in the [Iris Guide for
Users](iris-for-users.md).

### Navigational Items at the Top

There are a few navigational items at the top of the webpage.

![Iris menu bars](images/iris_elvis_menubars.png)
{: align="center" style="border:1px solid black"}

-   The "**hamburger**" icon (the one with three horizontal lines)
    contains a list of submenus.  When clicked, they are revealed
    in the left sidebar. To remove the sidebar, click 'X'.

    ![Iris menu bars](images/iris_hamburger_submenus.png)
    {: align="center"}

    A couple of UI (user interface) configuration options are
    provided for flexible handling of the contents with the hamburger
    submenus by various devices.  Please see the [UI
    settings](#iris_ui_settings) section.

-   The **Iris** icon next to it is clickable and serves
    as the Home button. This brings you back to the "Homepage" which
    is your account's [Profile](#profile) page.

-   The **search box** allows you to quickly get information that
    you want about an individual user or a project.

-   Your account name is displayed in the top right corner and is
    clickable. It brings you to your account's [Profile](#profile)
    page.

-   To logout, click on the hamburger icon, and then the **Logout**
    button.

For certain menu items, there is also a sub menu bar under the top
menu bar with several menu tabs. When this bar appears, the first
value on the left is to give you the context for the page displayed.
For example, if the page content is about your account itself (that
is, when the Iris icon is selected), the label will be your full
name, as shown above. If the content is about a project account,
it will display the project's name.

These menu bars are fixed in the page and do not scroll away even
when you scroll down the page.

### Sorting the Display Data

Displayed data can be sorted by a column by clicking on the column
label in the table.  By default data is sorted in the ascending
order. Clicking on the label again, you can toggle the sorting
order.

A gray line appears beneath or above the column label to indicate
the table column that the displayed data is sorted by and the
sorting order.

---

## Check Account Usage

### Check a User's Account Usage

To look up a user's account usage:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Go to the 'Per-User compute allocations' section.
3.  Find the row for the user.

Alternatively,

1.  Enter the username in the search box at the top right corner
    of the Iris webpage.
2.  Select the user from the search results.
3.  Click the 'CPU' and 'GPU' tabs.

### View Node Hour Usage by Users

To view Node Hour usage by members of your project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar. Note that the 'CPU' tab is selected by default.
    If you want to check GPU Node Hour usage, click the 'GPU' tab.
2.  Usage by users is provided in the 'Per-User compute allocations'
    table beneath the usage plot.

### View Resource Transfers To/From the Project

When viewing the allocation and usage summary at the top of the CPU
or GPU tab, if there is a difference between the number of hours
in the ERCAP Award (on the far right-hand side of the page) and the
Current Allocation (at the top left-hand side of the page), then
time has either been added to or taken away from the project. To
view these transfers, look below the Usage plot immediately below
the Summary and there will be a link to the "Allocation transfer
report".

![CPU tab Allocation Transfer report link](images/iris_projects_CPU_Allocation_Transfer_report_link.png)
{: align="center"}

The report provides information on: 

1. The Resource Type that was transferred (MPP - Compute Hours, HPSS - Archival Storage)
2. The person performing the transfer
3. The Amount of the transfer
4. Comments associated with the transfer
5. The date/Time of the transfer
6. DOE Program and/or project the transfer was performed to/from
   
![Allocation Transfer report](images/iris_projects_Allocation_Transfer_report.png)
{: align="center"}

### View CFS Storage Usage by Users

To view [CFS](../filesystems/community.md) storage usage by members
in a project and their quota:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Click the 'Storage' tab.
3.  Usage by users is provided in the 'CFS Directory Usage' table
    beneath the usage plot.

### View HPSS Charges and Usage by Users

To view the HPSS charges by members in a project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by users is provided in the 'HPSS Archive' table.

## Change a User's CPU/GPU Node Hour Quota

To change the user quota (the percentage or the number of CPU/GPU
Node Hours of a project's allocation that a user is allowed to use):

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar. Note that the 'CPU' tab is selected by default.
    If you want to edit the GPU Node Hour quota, click the 'GPU'
    tab.
2.  Usage by users is provided in the 'Per-User compute allocations'
    table beneath the account usage plot.
3.  Adjust the number in the box in either the 'Allocated Hours'
    box for a specific number of CPU/GPU Node Hours, or the 'Allocation
    % of Project' box for a percentage that the user is allowed to
    use. The 'Allocated Hours' field is only valid for MPP allocations.
    If both values are set, the percentage value will be used.
4.  Click the 'Update Allocations' button above the table.

## Enabling the premium QOS

To allow a user to use the ['premium' QOS](../jobs/policy.md#premium)
in running batch jobs:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Click the 'CPU' tab and go to the 'Per-User compute allocations'
    section.
3.  Click the pencil icon in the 'QOS' column for the user you want
    to give the premium QOS. This will show a pop-up menu.
4.  Select 'premium', and click OK. A black box with the label
    'premium' will show up in the QOS column.
5.  Click the 'Update Allocations' button above.

This setting applies to the user's GPU jobs, too.

## Adjust a Directory's Quota in the Community File System

To change a directory's [CFS (Community File
System)](../filesystems/community.md) quota for a user:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by CFS directories is provided in the 'CFS Directory
    Usage' table beneath the CFS Storage usage plot.
4.  Adjust the 'Directory Storage Quota (TB)' value and/or
    the 'Directory File Quota' value for the CFS directory whose
    space and/or inode quota you want to modify. Note that the sum
    of the 'Directory Storage Quota' column must be equal to or
    less than the 'Storage Allocation' value for the project, shown
    at the top of the page. Similarly, the sum of the 'Directory
    File Quota' column must be equal to or less than the 'Files
    Allocation' value.
5.  Click the 'Update Quotas' button above the table.

## Add a CFS Directory to Your Project

To add a new or existing [CFS (Community File
System)](../filesystems/community.md) directory to your project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by CFS directories is provided in the 'CFS Directory
    Usage' table beneath the CFS Storage usage plot.
4.  Click the '+ New' button above the table.
5.  Fill in the Directory name. This can be a new name or an existing
    directory you previously controlled.
6.  Choose a Unix Group. This can either be an existing Unix Group
    or a new Unix Group with the same name as the new directory.
7.  Enter the Owner username.
8.  Enter the number in the 'Directory Storage Quota (TB)' and/or
    the 'Directory File Quota'.
9.  Click the 'Save Changes' button.

!!! note
	Project directory names currently have a length limit of 8
	characters

## Change a User's HPSS Quota

To change the user HPSS quota:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by users is provided in the 'HPSS Archive' table at the
    bottom. Adjust the number in the '% Allowed by Project' box for
    a user.
4.  Click the ‘Update HPSS Allocations’ button above the table.

## Add a User to a Project

### For a User with an Active NERSC Account

To add an existing user who has an active NERSC account:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Click the '+ Add User' button.
4.  Type the user's name, username or email in the 'Select a user'
    box.
5.  Wait until Iris returns a list of the users matching the entered
    text. Select the user from the list.
6.  In the dialog box that appears:
    -   Set the project role for the user:
	`user`, `project_resource_manager`, `project_membership_manager`,
	or `pi_proxy`.
	- NOTE: There can be only one user with the PI role per
	  project.
    -   Set a NERSC hour quota in either the 'Allocated Hours' or
	'% of Project's Hours' box. If both are set, the percentage
	value will be used. Set the HPSS quota for the user in the
	'% of HPSS Storage' box.
    -   Click 'Save Changes.'
7.  Once you enter the data for the user's account, it will added
    to your project and the user will be sent an email notification.

### Add Users from the Previous Year

You can add back all or some of the users who were in the project
in the previous allocation year but not the current year. For
security reasons, only active users can be added this way.

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Click the 'From Last Year' button.
4.  Check the boxes next to the user names in the dialog box's
    table.
    -   Set the project role for the users:
	`user`, `project_resource_manager`, `project_membership_manager`,
	or `pi_proxy`.
	- NOTE: There can be only one user with the PI role per
	  project.
    -   Set a CPU/GPU Node Hour quotas in either the 'User Allocation'
	or 'Allocation %' box. If both are set, the percentage value
	will be used. Set the HPSS quota for the users in the 'HPSS
	%' box.
    -   Click 'Add Selected Users.'
5.  When asked to confirm that you want to add the users, click OK.
6.  After you have approved the account request, the added users
    will receive an email notification.

### For a New User to NERSC or an Existing User with a Deactivated Account

To add a new user or a user with a deactivated NERSC account, you
have to invite the user.  Please follow the steps below.

1.  Go to Iris invite-user page:
    [https://iris.nersc.gov/invite-user](https://iris.nersc.gov/invite-user)
2.  Type either PI name or a project name, and select the project
    for the user to join.
3.  Type the user's email address.
4.  Click the 'Submit' button.
5.  The user will receive an email notification.

### Reviewing a User Request to Join a Project

Once the user has submitted a request and their account has completed 
the compliance vetting, PIs and PI Proxies (that is, the project managers
with the 'Accept Users' permission) will be notified about the
submission.  Any of them may then log into Iris and approve the
account request, following the steps below.

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  You will see the pending requests in the 'Pending Members'
    table (you may need to scroll down the page).  Review the information 
    provided by the user to be sure they are a valid user who needs an 
    account under your project.

    ![CPU tab Allocation Transfer report link](images/iris_projects_roles_pendingmembers.png)
    {: align="center" style="border:1px solid black"}

4.  If everything looks OK, click on the <span style="color:green">**green**</span> Thumbs Up button.
    In the dialog box that appears:
    -   Set the project role for the user: `user`,
        `project_resource_manager`, `project_membership_manager`,
	or `pi_proxy`.
        - NOTE: There can be only one user with the PI role per project.
    -   Set CPU/GPU Node Hour quotas in either the 'Allocated
	Hours' or '% of Project Allocated' box. If both are set,
	the percentage value will be used. Set the HPSS quota for
	the user in the '% of HPSS Storage' box.
    -   Click the 'Save Changes.'
5.  If you do not know this person, you can click on the <span style="color:red">**red**</span> Thumbs Down
    button.
6.  After you have approved the account request, the approved user
    will receive a Welcome email and an additional containing a link that 
    will allow the user to set their initial password.

## Delete a User from a Project

To delete a user from a project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Find the row for the user you wish to delete, and click the
    row.
4.  Click the '- Remove User' button.

Keep in mind that if a user is a member of more than one project,
removing them from only your project will allow their account to
remain active in order to access the other project(s). If they are a
member of only your project, when you remove them, their account will
start a process of disabling their access allowing them time to move
or archive files. 

!!! tip
	Please see NERSC's [data retention policy](../policies/data-policy/policy.md).

## Batch Add/Remove Users to or from a Project

You can add several users to a project at once. Similarly, you can
remove several users from a project. To add or remove several users
to or from your project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Click 'Batch add/remove users'.
4.  In the dialog box, select the desired batch operation.
5.  Enter usernames, one name per line.
6.  You can optionally enter notes that will be sent out to the users.
7.  If you are adding users:
    -   You can enter a QOS name (e.g., `premium`) to give all of
        them access to it, if necessary.
    -   Set CPU/GPU Node Hour quotas in either the 'Allocated Hours'
	or '% of Project Allocated' box. If both are set, the
	percentage value will be used. Set the HPSS quota for the
	user in the '% of HPSS Storage' box.
    -   Click the 'Save Changes.'
    -   The project role for the users will be a `user`.

## Change a User's Project Role

To change a user's role in a project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Find the row for the user you wish to edit the roles. In the
    Role column, change the user's role to one of `user`,
    `project_resource_manager`, `project_membership_manager`,
    `pi_proxy`, and `pi`.
4.  Click the 'Update All' button.

## Update a User's Organization Information

Please ask your users to keep their contact information up to date
because project managers cannot update the information in Iris for
other members.

But PIs can update organization information for a member if there
is a change (of course, the user can update the information by
himself/herself). To update the information:

1.  Enter the user name or username in the search box in the top
    right corner.
2.  Select the user from the search results.
3.  Note that the 'Profile' tab is selected for the user account.
4.  Start to type the correct organization name in the 'Select
    an organization' field in the 'Self-service User Info' section.
    Select the correct one from the returned list of institutions
    with the matching words.
5.  Enter the new email address for the user in the 'Primary Email'
    field. The email address should have the correct domain name
    for the institution.
6.  Click the 'Update' button.

## Add a User to a Unix Group

To add a user to a Unix group:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Groups' tab.
3.  Choose a desired Unix group from the box on the left.
4.  Type the user's name, username or email in the 'Add a user'
    box.
5.  Click the '+ Add User' button. Verify that the user appears in
    the member table for the group.

## Delete a User from a Unix Group

To delete a user from a Unix group:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Groups' tab.
3.  Choose a desired Unix group from the box on the left.
4.  Check the box next to the user that you want to remove from the
    group.
5.  Click the '- Remove Users' button. Check the user is removed
    from the member table for the group.

## Set Your User List for the Next Allocation Year

This interface is available for PIs, PI-proxies and Project Membership
Managers, only in the month or so before the new allocation year
starts. To review and set user statuses for the next allocation
year:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Keep the checkbox in the 'Cont. User?' column checked only for
    the continuing users.
4.  Check the box in the 'Allow Premium?' column for the users that
    can run in the premium QOS.
5.  Click the 'Update All' button.

If you want to mark all users in a project as continuing users,
simply click the 'Toggle All Cont. Users' text on the right.

Similarly, you can select or deselect all users for the premium
QOS, by simply clicking the 'Toggle All Allow Premium' text on the
right.

Please keep in mind that for security reasons only active users
should keep their accounts.

## Add Publications To a Project's Page

To add a publication to project's Publications section:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Details' tab.
3.  Click the '+ Add Publication' button in the Publications section
    at the bottom of the page.
4.  In the Claim Publication box, enter the publication's DOI number
    and the project year at NERSC.
5.  Click the Begin button.
6.  To claim this publication, click the '+ Claim' button.
