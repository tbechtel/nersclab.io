# Workflow nodes

!!! Warning "Cori workflow node retirement"
    The Cori workflow nodes will retire along with the rest of the Cori system,
    which is scheduled to take place on January 17, 2023.

    We encourage Cori workflow node users to migrate their workloads to the
    [Perlmutter workflow queue](./workflow-queue.md) as soon as possible to
    debug any issues.

We know that traditional supercomputing centers (including NERSC) are sometimes
not ideal for running long-lived scientific workflow tools. Batch jobs and
queue wait times can be obstacles to effectively using these tools.

We understand these difficulties! In our effort to better support workflows,
Cori has two dedicated nodes especially for running workflow software:
`cori20` and `cori21`.

!!! Warning "Workflow nodes are for orchestrating workflows"
    **NERSC workflow nodes are for coordinating your workflow, not
    running your workflow.** What does this mean? Having a cronjob
    that wakes up once an hour, checks for data, and launches
    jobs on the compute nodes is an excellent use case for the
    workflow nodes. Actually
    running your computations on the workflow nodes
    is prohibited.

Like other login nodes at NERSC, the workflow nodes are shared resources.
Please adhere to the NERSC login node
[guidelines](../../connect/index.md#login-nodes) and be a good citizen.

## How can I get access to the Cori workflow nodes?

Please submit a ticket at help.nersc.gov

To help us decide if your use case is appropriate for the workflow nodes,
you will be asked:

```
User:
Email:
Repository Name:
Purpose:
Estimated memory usage:
Estimated CPU usage:
Estimated Data usage:
Estimated I/O:
Frequency / length of process:
Need external resources:
```

## Workflow node details

`cori20` and `cori21` are similar to the rest of the NERSC login nodes. They 
are Haswell nodes with 500 GB of memory per node. Once you have logged onto 
Cori, you can reach them via

```
ssh coriwork
```

A load balancer will then direct you to either `cori20` or `cori21`. From Cori, 
you can also ssh directly to `cori20` or `cori21`.
