# Rancher 1

Originally launched in May 2018, this
version of Rancher is based on Docker Compose technology and is
entirely driven by command-line interface and YAML markup.

**Rancher 1 is currently unsupported. All Spin users are
strongly encouraged to migrate their services from Rancher 1 to
Rancher 2 as soon as possible.**
