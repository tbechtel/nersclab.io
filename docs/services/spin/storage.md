# Storing Data in Spin

Docker is great for running a service and Rancher is great for making sure your service is always available. However,
since Docker containers are ephemeral, if  your application needs to store data that's available after a restart, you
will need to create a persistent volume.

In Rancher, you can create a volume mount when deploying a new service, (or editing an existing one) under the `Volumes`
panel. Click `Add Volume` and you will see choices for the various types of volumes Rancher offers.

Below we'll discuss the various types of storage available to you in Spin. Each type of storage has pros and cons that
you should consider before using it.

## NFS Client: fast, local to Spin

Persistent volumes using the _NFS Client provider_ use a high-performance networked storage appliance accessible to Spin
nodes. Storage here is fast and available across nodes and restarts. It's a great choice for general application state
storage, for example, for a database.

Give the persistent volume a name, under `Storage Class` select `nfs-client`, and give it a capacity of 1Gb. On the next
screen you can enter a mount point which is where storage will be accessible inside your container. The mount point
should be an absolute path (eg.: `/my_app_data`).

## Global File Systems: large-scale, accessible outside Spin

Mounting a NERSC Global File System (NGF) in Spin, such as the Community File System, allows you to access your files
both in your Spin app and on other NERSC systems. This storage option is suitable for large data files but is not
suitable for heavy transactional IO load.

To configure an NGF mount, choose `Bind mount a directory from the node` and next to `The Path on the Node must be`,
select `An existing directory`. This storage option is suitable for large data files but is not suitable for heavy
transactional IO load. Global File Systems have the benefit of also being visible when you are using other NERSC
resources. 

Using these external volumes has additional requirements that you must configure: in the `Command` panel:

- In the `User ID` box, you must enter your user id or the id of a collab user you can become
- In the `Filesystem Group` box, you must enter the id of a group you belong to

These settings will cause Rancher to run your Docker image as the user specified, ensuring that access to data is
secured.

Additionally, make sure that the directory in GPFS has the `o+x` permissions from the root path to the mount point.

The following Global File Systems are available in Spin:

|Path on Spin node|Access type|
|-----------------|-----------|
|/cvmfs           |read-only  |
|/global/common/  |read-only  |
|/global/dna/     |read-only  |
|/global/cfs/     |read/write |
|/global/homes/   |read/write |
|/global/u1/      |read/write |
|/global/u2/      |read/write |

When creating an external volume mount, the `path on node` value must start with one of the paths in the above table.
Set `The path on the node must be` drop-down to `an existing directory`. The mount point in the container can be any
absolute path.

## Secrets

Another option in the `Add Volume` drop-down is to use a secret. A secret is a piece of information you want your
running container to have, but don't want others looking at your Spin configuration to see. For example, you could
specify the database password as an environment variable, but then anyone seeing the configuration would see it.

A better way is to create a secret. This can be done in the Rancher UI before you deploy your application. Click the
`resources` menu, select `Secrets` and on the `Secrets` tab, click `add secrets`. Give the secret a name and enter the
information you want to keep secret (typically a name and value pair). Then when deploying your application, you can
create a volume mount of type `use secret` and specify where on the filesystem in your container the secret file should
be stored.
